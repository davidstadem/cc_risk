# %%
from datetime import datetime
import sqlite3 as sql
import pandas as pd

# %%
conn = sql.connect('data/FloodClaims.db')

# %%
sum_columns = [
    'yearOfLoss',
    'amountPaidOnBuildingClaim',
    'amountPaidOnContentsClaim',
    'amountPaidOnIncreasedCostOfComplianceClaim',
    'totalBuildingInsuranceCoverage',
    'totalContentsInsuranceCoverage',
]
# %%
columnssql = ','.join(sum_columns)
statement = f'SELECT {columnssql} FROM flood;'
# %%
df=pd.read_sql(statement, conn)
df['yearOfLoss'] = pd.to_numeric(df['yearOfLoss'],downcast='integer')
for c in sum_columns[1:]:
    df[c] = pd.to_numeric(df[c],errors='coerce')
df
# %%
dfy=df.groupby('yearOfLoss').sum()
# %%
dfy.to_sql('flood_year',conn)
# %%
