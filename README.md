This is a repository to estimate the social cost of carbon via risk analysis data for the US.


# Requirements:
- pandas
- numpy (but that's included with pandas)
- plotly

# Usage:

Make a directory called `plots`.

Run `flood_risk_slope.py`.

Here's what it looks like on the command line:
```
> mkdir plots
> python flood_risk_slope.py
```

# Datasets in the Repository

Data was loaded into separate Excel files for each decade.
The Excel files, and the .db file, are too big to put in this repository.

Data was put into `FloodClaims.db` using `concat_flood_files.py`.
The output of `concat_flood_files.py` can be found in `concat_flood_files_output_20220312.txt`.

Once the data was all in `FloodClaims.db`, just yearly data was exported to `flood_claims_by_year.csv`.

```
sqlite3.exe -header -csv .\FloodClaims.db "SELECT * FROM flood_year;">flood_claims_by_year.csv
```

If you don't have `FloodClaims.db` available, maybe you can reconfigure `

# Methods:

Time-series data on flood insurance payouts were obtained from the OpenFEMA dataset.

The data were plotted by year.

Data showed an upward trend over time.

# Normalizing Damages

Damages to property reported in these types of datasets are confounded by multiple factors.
Pielke and Landsea (1998) attempted to normalize hurricane damages in the US and identified three primary normalizing factors:
1. Inflation
2. Wealth growth per capita
3. Population growth

Using these normalizing factors, they estimated that the 1938 New England Category 3 hurricane that caused roughly $300 million in damage would have caused over $16 billion in normalized damages in 1995.

We used the following:

## Inflation

Inflation values were taken from the Bureau of Labor Statistics (CITE BLS) Consumer Price Index for all urban consumers (CPI-U, BLS series CUUR0000SA0).
CPI was averaged per year.

## Wealth
Wealth data was obtained by Pielke from the US Bureau of Economic Analysis as "per-capita fixed reproducible tangible wealth" . The data was extrapolated out from 1980-1989 to obtain estimates from 1990 onwards.
Only data from 1925-1989 was obtained.

In our research, per-capita
I couldn't find current "fixed reproducible tangible wealth" data. 
There's an updated report through 1995, but extrapolating that data through 2022 seems ill-advised.
There is a database in Excel from census.gov that goes up to 2003 but I can't even confirm that I can reproduce that dataset from BEA.

Hamilton and Liu (2014 https://doi.org/10.1093/oxrep/gru007) found that System of National Accounts data only accounted for wealth up to 2.5-6 times GDP, where real wealth should be more on the order of 20 times GDP.
They ran some analyses that found that human capital, which is intangible, represents up to 60% of the total wealth of a nation. 
This data is not normalized for human capital.

The System of National Accounts (SNA) is used.
SNA appears to be the new standard.

https://unstats.un.org/unsd/nationalaccount/docs/SNA2008.pdf
Page 555 shows produced non-financial assets.
This is category AN1.

The data explorer from UNSTATS was super un-helpful. I couldn't find category AN1 anywhere.
https://unstats.un.org/unsd/nationalaccount/sdmxdata/nonfinancial

I finally found some data from OECD.org that appear analogous.
https://stats.oecd.org/Index.aspx?datasetcode=SNA_TABLE9B_ARCHIVE#
Category N1: 

To keep using SNA data, I would need to harmonize the pre-2019 data with post-2019 data.
I downloaded data from 1980 onward, but I need to download up to 2022 data.
However, I found another source of data: the Fed.

### But Wait, There's More!

I found the following from the Fed:
https://www.federalreserve.gov/datadownload/Download.aspx?rel=Z1&series=f9a5e21e90ff82b038b7e8f930aa0b58&lastObs=100&from=&to=&filetype=csv&label=include&layout=seriesrow

This was from the Fed's Z.1 Statistical Release, Thursday, March 10, 2022.
- Nonfinancial corporate business; nonfinancial assets
- Unique Identifier: Z1/Z1/FL102010005.A

I found that the Federal data is pretty close to the SNA data (see compare_wealth.py).
So we'll use the fed.

Next Steps: but I still need to get *per-capita* wealth.
This is inflation-adjusted wealth, but it's not *per-capita inflation-adjusted* wealth.

## Population growth
Should be readily available. Census data, or I think I can get that from BLS as well.

obtained from fed
https://fred.stlouisfed.org/series/POPTOTUSA647NWDB
