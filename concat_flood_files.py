# %%
from datetime import datetime
import sqlite3 as sql
import pandas as pd
from openpyxl import load_workbook

# %%
floodxlfiles = {
    'FloodClaims80s.xlsx' : 1980,
    'FloodClaims90s.xlsx' : 1990,
    'FloodClaims00s.xlsx' : 2000,
    'FloodClaims10s.xlsx' : 2010,
}
# %%
conn = sql.connect('data/FloodClaims.db')

COLUMNS=['agricultureStructureIndicator', 'asOfDate', 'baseFloodElevation',
       'basementEnclosureCrawlspace', 'condominiumIndicator',
       'policyCount', 'countyCode', 'communityRatingSystemDiscount',
       'dateOfLoss', 'elevatedBuildingIndicator',
       'elevationCertificateIndicator', 'elevationDifference',
       'censusTract', 'floodZone', 'houseWorship', 'latitude',
       'longitude', 'locationOfContents', 'lowestAdjacentGrade',
       'lowestFloorElevation', 'numberOfFloorsInTheInsuredBuilding',
       'nonProfitIndicator', 'obstructionType', 'occupancyType',
       'originalConstructionDate', 'originalNBDate',
       'amountPaidOnBuildingClaim', 'amountPaidOnContentsClaim',
       'amountPaidOnIncreasedCostOfComplianceClaim',
       'postFIRMConstructionIndicator', 'rateMethod',
       'smallBusinessIndicatorBuilding', 'state',
       'totalBuildingInsuranceCoverage', 'totalContentsInsuranceCoverage',
       'yearOfLoss', 'reportedZipcode', 'primaryResidence', 'id']
ncols=len(COLUMNS)
usecols =list(range(ncols))
# %%
from itertools import islice
d={}
start=datetime.now()
for xlfile, startyear in floodxlfiles.items():
    path = 'data/' + xlfile
    years=10
    if xlfile == 'FloodClaims10s.xlsx':
        years=12

    print('loading', xlfile)
    wb = load_workbook(path,read_only=True,keep_vba=False)
    for i in range(years):
        year=startyear+i
        elapsed = datetime.now()-start
        
        sheet = wb[str(year)]
        firstcell= sheet.cell(1,1).value
        row1=None
        data = sheet.values
        #sometimes the first row isn't the column headers
        if firstcell == 0:
            #don't skip the first row for those times
            pass
        elif firstcell == COLUMNS[0]:
            #skip first row most of the time
            next(data)
        else:
            raise("uhoh columns what?")
        
        #slice out only the first 39 columns
        data = (islice(r, None, ncols) for r in data)

        dfi = pd.DataFrame(data,columns=COLUMNS)
        #drop any rows where 'id' is nothing - 
        # this happens for some 1980s workbooks
        # because there's one calculated formula way at the bottom of the worksheet
        
        dfi = dfi[pd.notnull(dfi['id'])]
        tup= (elapsed.seconds, xlfile, year, dfi.iloc[0,1],dfi.iloc[-1,-1])
        print(tup)
        dfi.to_sql('flood',conn,if_exists='append')

    
    wb.close()


# %%
