import pandas as pd
import plotly.express as px
import sqlite3 as sql
DEFAULT_RANGE = (1978,2022)

DEFAULT_SQL = 'select yearOfLoss,amountPaidOnBuildingClaim,amountPaidOnContentsClaim FROM flood_year'
SQLDBPATH = 'data/FloodClaims.db'
LOSSESCSVPATH = 'data/flood_claims_by_year.csv'


def get_actual_losses_sql(sqlstatement=DEFAULT_SQL):
    conn = sql.connect(SQLDBPATH)
    df=pd.read_sql(sqlstatement,conn)
    df.columns = ['Date','BuildingPaid','ContentsPaid']
    df['BuildingPaid'] = pd.to_numeric(df['BuildingPaid'], errors='coerce')
    
    return df
def get_actual_losses_csv(csvpath=LOSSESCSVPATH):
    df=pd.read_csv(csvpath)
    cols = {
        'amountPaidOnBuildingClaim' : 'BuildingPaid',
        'amountPaidOnContentsClaim' : 'ContentsPaid',
        'yearOfLoss' : 'Date',
    }
    df=df[cols.keys()].rename(columns=cols)
    return df

def get_actual_losses(sqlstatement=DEFAULT_SQL,csvpath=LOSSESCSVPATH):
    from os.path import exists
    if exists(SQLDBPATH):
        return get_actual_losses_sql(sqlstatement)
    
    return get_actual_losses_csv(csvpath)

def get_wealth_sna(wealth_path=None, index_to=1980):
    if wealth_path is None:
        wealth_path = 'data/SNA_TABLE9B_ARCHIVE_11042022225823962.csv'
    df = pd.read_csv(wealth_path)
    ser_wealth = df[['Year','Value']].groupby('Year').sum()['Value']
    ser_wealth.name = 'SNA'

    if index_to:
        ind = ser_wealth.loc[index_to]
        ser_wealth = ser_wealth/ind

    return ser_wealth

def get_wealth_fed(wealth_path=None, index_to=1980):
    if wealth_path is None:
        wealth_path = 'data/fed_reserve_nonfinancial_assets.csv'
    ser_wealth = pd.read_csv(wealth_path,index_col=0,squeeze=True)
    ser_wealth.name = 'Fed'

    if index_to:
        ind = ser_wealth.loc[index_to]
        ser_wealth = ser_wealth/ind

    return ser_wealth

def get_cpi(cpi_path=None, index_to = 1980):
    '''retrieves CPI from BLS data.
    CPI = U.S. city average, series ID: CUUR0000SA0
    https://data.bls.gov/timeseries/CUUR0000SA0
    '''
    if cpi_path is None:
        cpi_path = 'data/BLS_CUUR0000SA0_1913_2022.xlsx'

    dfcpi=pd.read_excel(
        cpi_path,
        skiprows=11,
        index_col=0,
        usecols=list(range(13)))
    cpi = dfcpi.mean(axis=1)

    if index_to:
        ind = cpi.loc[index_to]
        cpi = cpi/ind

    return cpi

def get_population(pop_path=None, index_to=1980):
    '''obtained from fed
    https://fred.stlouisfed.org/series/POPTOTUSA647NWDB
    '''
    if pop_path is None:
        pop_path = 'data/POPTOTUSA647NWDB.csv'
    ser_pop = pd.read_csv(pop_path, index_col=0, parse_dates=[0],squeeze=True)
    ser_pop.name = 'pop_fed'
    ser_pop.index = ser_pop.index.year
    
    if index_to:
        ind = ser_pop.loc[index_to]
        ser_pop = ser_pop/ind
    
    px.line(ser_pop)
    return ser_pop


def plot_wealth(df, title,layout=None):
    fig=px.line(df,title=title)
    fig.update_layout(layout)
    fig.update_xaxes()
    fig.show()
    filename = f"plots/{title}.png"
    fig.write_image(filename,scale=3)
    return fig

def plot_pop():
    ser=get_population(index_to=None)
    fig=px.line(ser)
    fig.update_layout(
        yaxis_title='Population, millions',
        showlegend=False,
        xaxis_title='Year',
        xaxis_range=DEFAULT_RANGE,
    )
    fig.write_image('plots/pop.png',scale=3)
    return fig

def plot_cpi():
    ser=get_cpi(index_to=None)
    fig=px.line(ser)
    fig.update_layout(
        yaxis_title='Consumer Price Index',
        showlegend=False,
        xaxis_title='Year',
        xaxis_range=DEFAULT_RANGE,
    )
    fig.write_image('plots/cpi.png',scale=3)
    return fig

def main():
    layout=dict(
        xaxis_range=DEFAULT_RANGE,
        yaxis_title='US Dollars',
        yaxis_tickprefix='$',
        legend_title=None,
    )
    sersna = get_wealth_sna(index_to=None)
    serfed = get_wealth_fed(index_to=None)

    df = pd.concat([sersna,serfed],axis=1)
    plot_wealth(df,title='Total Wealth',layout=layout)

    layout['yaxis_tickprefix']=None
    layout['yaxis_title']='Wealth Index (base=1980)'
    sersna = get_wealth_sna(index_to=1980)
    serfed = get_wealth_fed(index_to=1980)

    df = pd.concat([sersna,serfed],axis=1)

    plot_wealth(df, title='Total Wealth, Indexed to 1980',layout=layout)

    plot_cpi()
    plot_pop()

if __name__ == '__main__':
    main()
    # Doesn't look like a significant difference - cool, we'll use the Federal dataset.