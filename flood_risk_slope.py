import plotly.express as px
import pandas as pd
import numpy as np
from datasets import get_wealth_fed, get_wealth_sna, get_cpi, get_population, get_actual_losses



INDEX_YEAR=1980

def write_fig(fig, name):
    fig.update_layout(
        width=6*96,
        height=4*96
    ).write_image(f"plots/{name}.png",scale=3)

def plot_yearly(df_orig):
    dfyear = df_orig.set_index('Date')
    dfyear.columns=['Building','Contents']

    fig=px.area(dfyear)

    layout=dict(
        yaxis_title='Losses, US Dollars',
        title='Insurance Payouts, OpenFEMA Database',
        xaxis_title='Year',
        legend_title=None,
        yaxis_tickprefix = '$', 
    )
    fig.update_layout(layout)

    fig.update_traces(mode='lines+markers')
    write_fig(fig, 'Yearly_area')

    fig=px.line(dfyear.sum(axis=1))

    fig.update_yaxes(type='log')
    layout['showlegend']=False
    fig.update_layout(layout)
    fig.update_traces(
        line_color='black',
        mode='lines+markers',
    )

    write_fig(fig, 'Yearly_line_total')


def get_wealth(wealth_path=None, index_to=INDEX_YEAR, method='fed'):
    if method == 'fed':
        return get_wealth_fed(wealth_path, index_to)
    elif method == 'sna':
        return get_wealth_sna(wealth_path, index_to)
    else:
        return 'method must be "fed" or "sna"'

def calculate_real_percapita_wealth(df, index_to=INDEX_YEAR):
    index_cpi = df.loc[index_to, 'CPI']
    df['WealthPerCapita'] = df['Wealth']/df['Pop']
    df['WealthPerCapitaReal'] = df['WealthPerCapita']/df['CPI']*index_cpi
    return df

def normalize(df, base=INDEX_YEAR):
    '''
    - df: data frame - index=year, has columns I need
    - base: either a single year or tuple (startyear, stopyear)
        this is the basis period for normalization
    '''
    ser=None
    if type(base) is tuple:
        fromyear,toyear=base
        ser=df.loc[fromyear:toyear,:].mean()
    else:
        ser=df.loc[base]
    
    base_claims = ser['Claims']
    base_pop = ser['Pop']
    base_wealth=ser['WealthPerCapitaReal']
    base_cpi=ser['CPI']

    df['Base'] = base_claims
    df['Inflation'] = base_claims/base_cpi*df['CPI']
    df['Inflation+Population'] = df['Inflation']/base_pop*df['Pop']
    df['Inflation+Population+Wealth'] = df['Inflation+Population']/base_wealth*df['WealthPerCapitaReal']
    pass

def calculate_climate_change_impact(ser, base=INDEX_YEAR, rate_cc=0.01):
    '''
    - ser: Series - index=year, values=losses before climate change
    - base: either a single year or tuple (startyear, stopyear)
        this is the basis period for normalization.
        You only need this for normalizing.
    - rate_cc: percentage change per year due to climate change
        e.g. 0.04 for 4% avg. increase year-over-year
    '''
    
    k = np.log(1+rate_cc)
    t = ser.index-base
    multiplier = np.exp(k*t)
    
    ser_return = ser*multiplier
    return ser_return

# %%
def plot_sim_claims(df,cols=None,kwargs=None):
    if kwargs is None:
        kwargs={}
    if cols is None:
        cols = ['Base','Inflation','Inflation+Population','Inflation+Population+Wealth']
    
    fig=px.line(df,
        y=cols,
        **kwargs
    )
    fig.update_yaxes(
        type='log'
    )
    fig.update_layout(
        yaxis_title='US Dollars',
        legend_title=None,
        yaxis_tickprefix = '$', 
        #yaxis_tickformat = ',.'
    )

    return fig



def plot_claims(dfc):
    colors = px.colors.sequential.Plasma.copy()
    #last=colors[-1]
    colors=colors[-2::-2]
    colors.insert(0,'#000000')
    #colors.append(last)
    fig = plot_sim_claims(
        dfc,
        cols=dfc.columns,
        kwargs=dict(color_discrete_sequence=colors),
    )

    fig.update_traces(
        selector=dict(name='Actual'),
        #line_color='black',
        mode='lines+markers',
    )
    return fig
# %%

def root_msle(pred,act):
    logdiff= (np.log(act)-np.log(pred))
    sqle = logdiff**2
    return sqle.mean()**0.5

def rmse(pred,act):
    diff= act-pred
    sqe = diff**2
    return sqe.mean()**0.5

def mae(pred,act):
    diff= act-pred
    mean_abs_error = np.abs(diff).mean()
    return mean_abs_error

def calculate_errors(df, index_year=INDEX_YEAR, plot=True):

    error_methods = [
        ('Root Mean Square Log Error', 'RMSLE', root_msle, (1.3,2)),
        ('Root Mean Square Error', 'RMSE', rmse, (1.73e9, 2.3e9)),
        ('Mean Absolute Error', 'MAE', mae, (0.76e9, 1.05e9)),
    ]
    d={}

    drates={}
    
    rates = np.linspace(0.0, 0.2)
    for cc in rates:
        pred = calculate_climate_change_impact(df['Inflation+Population+Wealth'],base=index_year, rate_cc=cc)
        drates[cc] = pred
    ser_rates_by_year= pd.concat(drates,names=['Rate','Year'])
    act=df['Claims']
    for title,short_title,error_method,yaxisrange in error_methods:
        errs = pd.Series({
            cc: error_method(drates[cc],act)
            for cc in rates
        })
        d[title]=errs
    dferrs = pd.concat(d,names=['Title','R']).rename('Error').reset_index()
    
    if plot:
        return plot_errors(dferrs, error_methods)
    
    return dferrs, error_methods, ser_rates_by_year

def plot_errors(dferrs, error_methods):
    fig=px.line(
        dferrs,
        x='R',
        y='Error',
        facet_row='Title',
    )
    fig.update_annotations(text='')
    i=0
    for title,short_title,error_method,yaxisrange in error_methods:
        fig.update_yaxes(
            row=3-i,
            col=1,
            range=yaxisrange,
            title=short_title,
            matches=None,
        )
        i+=1


    fig.update_layout(
        showlegend=False,
        xaxis_title='Climate Change Impact <em>r</em>, Year-over-Year',
        xaxis_tickformat='.0%',
        xaxis_range=(0,0.17),
    )
    fig.update_xaxes(
        dtick=0.02,
    )
    return fig

def meta_R(dfd:pd.DataFrame, df:pd.DataFrame):
    ser=(dfd.loc['50%']/df['Inflation+Population+Wealth'])
    px.line(ser)
    #y=y_0*exp(k*t)
    #ln(y)=ln(y_0)+k*t
    #k=(ln(y)-ln_y0)/t
    t=ser.index-1980
    A=np.vstack([t,np.ones(len(t))]).T
    m,b=np.linalg.lstsq(A,np.log(ser).values,rcond=None)[0]
    k=m
    R=np.exp(k)-1

    if False:
        #double-check work
        check=calculate_climate_change_impact(
            df['Inflation+Population+Wealth'],
            1988,R) # 1988 was about where the two lines cross,
        #But really the choice of year doesn't matter
        # we're only looking for the slopes to line up.
        years=dfd.columns
        fig=px.line()
        fig.add_scatter(x=years,y=dfd.loc['50%'])
        fig.add_scatter(x=years,y=df['Inflation+Population+Wealth'])
        fig.add_scatter(x=years,y=check)
        fig.update_yaxes(type='log')
        #trace 0 and trace 2 slopes should line up well here.
        #if so, we're good.

    print('Median R',R)
    pass


def plot_errors_yearly(dfallerrs:pd.DataFrame,df:pd.DataFrame, dfallrates:pd.DataFrame):
    dfrmsle = dfallerrs[dfallerrs['Title']=='Root Mean Square Log Error'].reset_index()

    dfminerr = dfrmsle.loc[dfrmsle.groupby('BaseYear')['Error'].idxmin()]
    fig=px.line(
        dfminerr,
        x='BaseYear',
        y='R',
    )
    write_fig(fig, 'rate_by_year')

    d={}
    fig=px.scatter()
    fig.add_scatter(x=df.index,y=df['Claims'])
    
    for i, row in dfminerr.iterrows():
        year=row['BaseYear']
        r=row['R']
        normalize(df,year)
        change=dfallrates.xs(year,level='BaseYear').xs(r)
        d[year]=change
        fig.add_scatter(
            x=df.index,
            y=change,
            name=year
        )
    fig.update_layout(
        yaxis_type='log',
    )

    write_fig(fig, 'bunchoflines')
    serminrates = pd.concat(d,names=['BaseYear','Year'])
    dfd=serminrates.unstack('Year').describe()
    
    meta_R(dfd,df)


    years=dfd.columns
    
    # %%
    fig=px.line(
    ).update_yaxes(
        type='log'
    ).add_scatter(
        x=years,
        y=dfd.loc['min'],
        line=dict(width=0),
        name='Min',
        showlegend=False,
    ).add_scatter(
        x=years,
        y=dfd.loc['max'],
        line=dict(width=0),
        name='Outer',
        fill='tonexty',
        fillcolor='lightgray',
        opacity=0.2,
    ).add_scatter(
        x=years,
        y=dfd.loc['25%'],
        line=dict(width=0),
        name='25%',
        showlegend=False,
    ).add_scatter(
        x=years,
        y=dfd.loc['75%'],
        line=dict(width=0),
        name='InterQuartile',
        fill='tonexty',
        fillcolor='gray',
        opacity=0.2,
    ).add_scatter(
        x=df.index,y=df['Claims'],
        line=dict(color='black'),
        mode='lines+markers',
        name='Actual',
    ).add_scatter(
        x=years,y=d[1980],
        line=dict(color='blue'),
        mode='lines',
        name='1980_base',
    )
    write_fig(fig, 'IQR')

    # %%


def main():
    print('reading df')
    df_orig = get_actual_losses()

    print('plotting')
    plot_yearly(df_orig)

    claims_raw = df_orig.set_index('Date').sum(axis=1)
    claims_raw.index.name='Year'
    claims_raw.name='Claims'

    df=claims_raw.to_frame()

    df['CPI']=get_cpi(index_to=None)
    df['Pop']=get_population(index_to=None)
    df['Wealth'] = get_wealth(index_to=None)
    df=calculate_real_percapita_wealth(df)
    normalize(df)
    plot_sim_claims(df).write_image('plots/simclaims.png',scale=3)
    
    dfc = pd.DataFrame().reindex(index=df.index)
    for rate in [0.00, 0.025, 0.05, 0.1, 0.15]:
        dfc['Actual'] = df['Claims']
        name = f"{rate:.1%}"
        dfc[name]=calculate_climate_change_impact(df['Inflation+Population+Wealth'] ,rate_cc=rate)
    
    plot_claims(dfc).write_image('plots/claims.png',scale=3)
    
    calculate_errors(df).write_image('plots/errors.png',scale=3)

    global INDEX_YEAR
    dallerrs={}
    dallrates={}
    for base_year in df.index:
        INDEX_YEAR = base_year
        normalize(df,base_year)
        dferrs, error_methods, ser_rates_by_year = calculate_errors(df,base_year, plot=False)
        dallerrs[base_year]=dferrs
        dallrates[base_year]=ser_rates_by_year

    dfallerrs = pd.concat(
        dallerrs,
        names=['BaseYear','Index'],
    )
    dfallrates=pd.concat(
        dallrates,
        names=['BaseYear','Rate','Year'],
    )
    dfrange = dfallrates.unstack('Year').describe()

    dfallerrs.to_excel('errors.xlsx')
    dfallerrs = dfallerrs.droplevel(1)
    
    plot_errors_yearly(dfallerrs,df, dfallrates)
    df.to_excel('df.xlsx')
    


if __name__ == '__main__':
    main()
# %%
